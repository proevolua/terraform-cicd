# KEY
resource "aws_key_pair" "proevolua" {
  key_name   = "proevolua-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCcQZXGgt1S0/XSWF2MmiBr7/aXEc6tk7M0HJlqk1FYNHKJh2XFm0Xcb+5flipXMk8b+uwOOv0HsAIf0YgvbIhhATcfBW6HxA/LsKq3oZ0nCVMJdbbChjbpvoE1OTt9RpnwRDJeiDX+uvmTjz6KHDgJWQOnyh5mGy4zL8rS9jfDujgJykql7jURuj8iLgsrcZW8sLpXeh+UtMHtBHJMfbA6CSXY8Qb61y47vCQI6KfDSy0wx35Ia8XPZfY6tlf7UVj+3A3sgft2qmHkRglPBMJayHfih3W1WRk7c2zBYtGxhudq+TSjMQgglqfKT4nQ087+OoDcaPRlqiB0sK1BUUct0g5EZLsPkjbdEKlww9impvJ6u1R3gNxEPGOFq3i5ScY2vsBmvrBqwqYp+G5bfSYeuTSlJRPLDswjy9Lw+OE/kjNHyZYexSAyrMb89lSd4BpUj+fShhrKapNuMQX+fMwNrTQkFqJsFGXMdw1MGZ4QkM5k8/as6AAicC+0poZksPM= proevolua"
}


# EC2
resource "aws_instance" "proevolua" {
  ami                    = "ami-052efd3df9dad4825"
  instance_type          = "t2.micro"
  key_name               = "proevolua-key"
  subnet_id              = aws_subnet.prod-subnet-public-1.id
  vpc_security_group_ids = [aws_security_group.ssh-allowed.id]

  tags = {
    Name = "teste-terraform-novo"
  }

}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.proevolua.id
  allocation_id = "eipalloc-00e420e76fa36d91e"
}