terraform {
  backend "s3" {
    bucket = "proevolua-aula-terraform-cicd"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}