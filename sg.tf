### EKS - Security Groups
module "sg-http-https-internet" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "5.0.0"

  name                = "ec2-dev-http-https-internet"
  vpc_id              = module.vpc.vpc_id
  ingress_with_self   = [{ rule = "all-all" }]
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp", "http-80-tcp"]
  egress_cidr_blocks  = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
}

module "sg-ssh-internet" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "5.0.0"

  name                = "ec2-dev-http-https-internet"
  vpc_id              = module.vpc.vpc_id
  ingress_with_self   = [{ rule = "all-all" }]
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-22-tcp"]
  egress_cidr_blocks  = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
}
